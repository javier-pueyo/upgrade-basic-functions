// Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];

function averageWord(mixedElements) {
  let counterAggregate = 0;
  for (const element of mixedElements) {
    if (typeof element === 'number') {
      counterAggregate += element;
    } else {
      counterAggregate += element.length;
    }
  }
  return counterAggregate;
}

console.log('La suma de los numeros y la longitud de los string es:',averageWord(mixedElements));