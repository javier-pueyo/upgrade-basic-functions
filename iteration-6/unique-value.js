// Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:

const duplicates = [
    'sushi',
    'pizza',
    'burger',
    'potatoe',
    'pasta',
    'ice-cream',
    'pizza',
    'chicken',
    'onion rings',
    'pasta',
    'soda'
  ];

  function removeDuplicates(duplicates) {
    let beforeItem;
    const noDuplicates = duplicates.map(function(currentItem,index,array) {
      let isDuplicate = false;
      array.forEach((iterator,indexIterator) => {
        if (!(indexIterator == index) && currentItem == iterator) {
          isDuplicate = true;
        }
      });
      if (isDuplicate) {
         array.splice(index, 1);
         index--;
         // No funciona, no consigo borrar el espacio vacio que transifere el map a la nueva array
      } else {
        return currentItem;
      }
      isDuplicate = false;
    });
    console.log(noDuplicates);
  }

function removeDuplicates2(duplicates) {
    const noDuplicates = [];
    for (let index = 0; index < duplicates.length; index++) {
      let isDuplicate = false;
      let currentItem = duplicates[index];
      // Estaba haciendo la función de includes en la anterior función sin darme cuenta xD
      if (!duplicates.includes(currentItem, index + 1)) {
        noDuplicates.push(currentItem);
      }
      // Tengo que ver como quitar los elementos duplicados del final, osea los últimos que se añadieron, no del principio
    }
    console.log(noDuplicates);
}

// FUNCIÓN FINAL: elimina los últimos elementos de la array duplicados. 
function removeDuplicates3(duplicates) {
    const noDuplicates = [];
    for (let index = duplicates.length - 1; index >= 0 ; index--) {
      let isDuplicate = false;
      let currentItem = duplicates[index];
      // No encontré la forma de que includes recorra la array al revés saltandose el actual item.
      // Solamente revisa a partir de iterator seleccionado.
      for (let indexIterator = index - 1; indexIterator >= 0 ; indexIterator--) {
        let iterator = duplicates[indexIterator]
        if (currentItem == iterator) {
          isDuplicate = true;
        }
      }
      if (!isDuplicate) noDuplicates.unshift(currentItem);
    }
    console.log(noDuplicates);
}

  removeDuplicates3(duplicates);