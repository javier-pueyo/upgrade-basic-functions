// Completa la función que tomando un array de strings como argumento devuelva el más largo, en caso de que dos strings tenga la misma longitud deberá devolver el primero.
// Puedes usar este array para probar tu función:

const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];

function findLongestWord(words) {
    let longWord;
    let indexlongWord;
    words.forEach(function(word, index) {
        if (!longWord) longWord = word.length;
        if (word.length > longWord) {
            longWord = word.length;
            indexlongWord = index;
        }    
    });
    return words[indexlongWord];
}

console.log(findLongestWord(avengers));
