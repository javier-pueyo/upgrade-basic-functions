// Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:

const numbers = [12, 21, 38, 5, 45, 37, 6];

function average(numbers) {
  let numberAverage = 0;
  for (const number of numbers) {
    numberAverage += number;
  }
  numberAverage = numberAverage / numbers.length;
  return numberAverage;
}

console.log(average(numbers));