// Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  Puedes usar este array para probar tu función:

const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];
  function repeatCounter(words) {
    words.forEach((currentWord , index) => {
      if (!words.includes(currentWord, index + 1)) {
        let counter = 0;  
        for (const word of words) {
          if (word === currentWord) counter++;
        }
        let wordVeces = (counter == 1) ? 'vez' : 'veces'; 
        console.log(`La palabra >> ${currentWord} << se utiliza ${counter} ${wordVeces}`);
      }
    });
  }
  
  repeatCounter(counterWords);